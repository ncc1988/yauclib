This directory contains test programs for the drivers which can
also be useful as examples on how to use the drivers.
They all come with an ucmf.config file for easy makefile creation
using the ucmf tool (see https://codeberg.org/ncc1988/ucmf).
