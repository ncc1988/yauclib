/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2023 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "../../../drivers/MAX7219.h"
#include "../../../drivers/LED.h"
#include "../../../Microcontroller.h"


int main()
{
    LED led;
    led.applyPinMapping(APIN::GENERAL, 7);
    MAX7219 display;
    display.applyPinMapping(APIN::SCK, 6);
    display.applyPinMapping(APIN::SDA, 2);
    display.applyPinMapping(APIN::ENABLE, 3);
    display.setEnabledDigitAmount(8);
    display.enable();

    led.setBrightness(1);
    Microcontroller::wait(500);

    while(true) {
        led.setBrightness(0);
        Microcontroller::wait(500);
        display.setBrightness(128);
        led.setBrightness(1);
        Microcontroller::wait(500);
        display.setSegment(1, 0xFF);
        display.setSegment(2, 0xAA);
        display.setSegment(3, 0xCC);
        display.setSegment(4, 0xF0);
        display.setSegment(5, 0x0F);
        display.setSegment(6, 0x33);
        display.setSegment(7, 0x55);
        display.setSegment(8, 0x00);
        Microcontroller::wait(500);
        display.setBrightness(64);
        Microcontroller::wait(500);
        display.setBrightness(128);
        for (uint8_t j = 0; j < 10; j++) {
            for (uint8_t i = 1; i < 9; i++) {
                display.setSegment(
                    i,
                    i % 2
                    ? (j % 2) ? 0x55 : 0xAA
                    : (j % 2) ? 0xAA : 0x55
                    );
            }
            Microcontroller::wait(500);
        }
        display.setBrightness(0);
        for (uint8_t i = 1; i < 9; i++) {
            display.setSegment(i, 0xFF);
        }
        for (uint8_t i = 0; i < 10; i++) {
            display.setBrightness(128);
            Microcontroller::wait(500);
            display.setBrightness(1);
            Microcontroller::wait(500);
        }
        display.setBrightness(64);
        Microcontroller::wait(500);
    }
}
