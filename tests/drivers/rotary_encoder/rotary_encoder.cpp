/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include <stdlib.h>


#include "../../../drivers/RotaryEncoder.h"
#include "../../../drivers/avr/AVRSerial.h"




int main()
{
    AVRSerial serial;
    RotaryEncoder encoder;
    serial.configureInterface(9600, true, true);

    encoder.applyPinMapping(APIN::SCK, 3);
    encoder.applyPinMapping(APIN::SDA, 2);

    serial.writeString("Ready\r\n");

    while (true) {
        //Very basic setup: Wait until the clock pin
        //has a falling edge followed by a rising edge.
        //Then take the measurement.
        while (Microcontroller::readPin(3));
        while (!Microcontroller::readPin(3));
        encoder.takeMeasurement();
        if (encoder.hasRotated()) {
            if (encoder.turnedClockwise()) {
                serial.writeString("->\r\n");
            } else {
                serial.writeString("<-\r\n");
            }
        } else {
            serial.writeString("--\r\n");
        }
    }
}
