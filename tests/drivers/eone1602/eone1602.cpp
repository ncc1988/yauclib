/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2022-2023 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "../../../drivers/EONE1602.h"
#include "../../../Microcontroller.h"


int main()
{
    EONE1602 display;
    #if defined UC_FAMILY_AVR
        #if defined UC_PACKAGE_ARDUINO_UNO
        //Use pins 2 - 13
        display.setRsPin(2);
        display.applyPinMapping(APIN::READ, 3);
        display.applyPinMapping(APIN::ENABLE, 4);
        display.applyPinMapping(APIN::D0, 5);
        display.applyPinMapping(APIN::D1, 6);
        display.applyPinMapping(APIN::D2, 7);
        display.applyPinMapping(APIN::D3, 8);
        display.applyPinMapping(APIN::D4, 9);
        display.applyPinMapping(APIN::D5, 10);
        display.applyPinMapping(APIN::D6, 11);
        display.applyPinMapping(APIN::D7, 12);
        display.setBacklightPin(13);
        #endif
    #endif

    display.backlightOn();
    display.initConnection(true);
    display.configure(true);
    //display.resetCursor();
    //display.clear();
    display.moveToPosition(0, 0);
    display.print("Hello world!");
    display.moveToPosition(1, 2);
    display.print("Ready!");
    display.moveToPosition(1, 10);
    display.printShort(1337);

    Microcontroller::wait(5000);

    display.clear();
    display.moveToPosition(0, 0);
    display.print("Analog:");
    display.moveToPosition(1, 0);
    uint16_t analog_read = 0;
    while (true) {
        //Read from an analog input:
        #if defined UC_FAMILY_AVR
            #if defined UC_PACKAGE_ARDUINO_UNO
        analog_read = Microcontroller::readAnalogPin(100, true);
            #endif
        #endif

        //Very simple clearing of a certain area:
        display.moveToPosition(1, 0);
        display.print("     ");
        //Write the value from the analog reading:
        display.moveToPosition(1, 0);
        display.printShort(analog_read);

        Microcontroller::wait(1000);
    }
}
