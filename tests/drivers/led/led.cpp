/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2023 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "../../../drivers/LED.h"
#include "../../../Microcontroller.h"


int main()
{
    LED led;
    #if defined UC_FAMILY_AVR
        #if defined UC_MODEL_ATTINY25 || defined UC_MODEL_ATTINY45 \
            || defined UC_MODEL_ATTINY85
        //The LED is connected to pin 6 (PB1).
        led.applyPinMapping(APIN::GENERAL, 6);
        #elif defined UC_PACKAGE_ARDUINO_UNO
        //Use the built-in LED of the Arduino Uno:
        led.applyPinMapping(APIN::GENERAL, 13);
        #endif
    #endif

    led.setBrightness(1);

    while (true) {
        Microcontroller::wait(500);
        led.setBrightness(0);
        Microcontroller::wait(500);
        led.setBrightness(1);
    }
}
