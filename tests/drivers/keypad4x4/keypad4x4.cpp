/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "../../../drivers/Keypad4x4.h"
#include "../../../drivers/avr/AVRSerial.h"
#include "../../../Microcontroller.h"


int main()
{
    AVRSerial serial;
    serial.configureInterface(9600, true, true);

    Keypad4x4 keypad;
    keypad.configure(Keypad4x4::POLLING_MODE, 1);

    //The pin mapping is for an Arduino Uno:
    keypad.applyPinMapping(APIN::D0, 2);
    keypad.applyPinMapping(APIN::D1, 3);
    keypad.applyPinMapping(APIN::D2, 4);
    keypad.applyPinMapping(APIN::D3, 5);
    keypad.applyPinMapping(APIN::D4, 6);
    keypad.applyPinMapping(APIN::D5, 7);
    keypad.applyPinMapping(APIN::D6, 8);
    keypad.applyPinMapping(APIN::D7, 9);

    char keys[5] = {0,0,0,0,0};
    char last_keys[5] = {0,0,0,0,0};

    while (true) {
        keypad.readKeys(4, keys);
        if (keys[0] != '\0' && keys[0] != last_keys[0] || keys[1] != last_keys[1]
            || keys[2] != last_keys[2] || keys[3] != last_keys[3]) {
            //At least one key has been read. Output all of them:
            serial.writeString(keys);
            //Set the last_keys array:
            last_keys[0] = keys[0];
            last_keys[1] = keys[1];
            last_keys[2] = keys[2];
            last_keys[3] = keys[3];
            //Reset the keys array:
            keys[0] = 0;
            keys[1] = 0;
            keys[2] = 0;
            keys[3] = 0;
        }
        Microcontroller::wait(100);
    }
}
