/**
 * This file is part of the yauclib library.
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "../../../../Microcontroller.h"
#include "../../../../drivers/avr/AVRSerial.h"
#include "../../../../drivers/LED.h"


int main()
{
    LED led;
    led.applyPinMapping(APIN::GENERAL, 13);

    AVRSerial serial;
    serial.configureInterface(9600, true, true);
    led.setBrightness(255);
    serial.writeString("Hello\r\n");
    while (true) {
        //Output everything that is received:
        serial.write(serial.read());
    }
}
