/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__MICROCONTROLLER_H
#define YAUCLIB__MICROCONTROLLER_H


#include <inttypes.h>


#include "definitions.h"
#include "SystemInterrupt.h"
#include "InterruptHandler.h"



class Microcontroller
{
    public:

    /**
     * The clock speed of the microcontroller in Kilohertz.
     */
    static const uint32_t clock_speed;

    //General I/O:

    /**
     * A configuration method to setup the analog digital converter
     * of a microcontroller.
     *
     * @returns bool True, if the configuration was successfull, false otherwise.
     */
    static bool setupAnalogReading();

    /**
     * @returns uint16_t The maximum value the microcontroller can read from
     *     an analog input pin.
     */
    static const uint16_t analog_input_max_value;

    /**
     * Writes the state of the pin (on or off).
     *
     * @param uint8_t pin The pin whose state to be set.
     *
     * @param bool value True, if the pin shall be set to on,
     *     false otherwise.
     */
    static void writePin(uint8_t pin, bool value);

    /**
     * Toggles the state of the pin (on becomes off and vice versa).
     *
     * @param uint8_t pin The pin whose state shall be toggled.
     *
     */
    static void togglePin(uint8_t pin);

    static void writeAnalogPin(uint8_t pin, uint16_t value);

    static bool readPin(uint8_t pin);

    /**
     * Reads a value from an analog pin.
     *
     * @param uint8_t pin The pin from which to read.
     *
     * @param bool normalise8 Whether to normalise the value in the 8-bit
     *     range from 0 to 255 (true) or to use the values the ADC unit of
     *     the microcontroller provides (false). Defaults to false.
     */
    static uint16_t readAnalogPin(uint8_t pin, bool normalise8 = false);

    static void setPinDirection(uint8_t pin, bool output = false);

    //Timer, delay and interrupts:

    static void wait(const uint16_t miliseconds);

    static void waitMicroseconds(const uint16_t microseconds);

    /**
     * UNSTABLE METHOD!
     * Adds an interrupt handler that shall be called when a timer
     * counted a specific amount of microseconds.
     *
     * @param const InterruptHandler* handler The handler to be called.
     *
     * @param uint16_t microseconds The amount of microseconds the timer shall wait.
     *
     * @param bool hi_res_timer Whether to use a high-resolution timer (true)
     *     or not (false). Defaults to false.
     */
    static void addTimerInterruptHandler(
        InterruptHandler* handler,
        uint16_t microseconds,
        bool hi_res_timer = false
        );

    /*
    virtual void setPinInterruptHandler(PinInterruptHandler* handler, uint8_t pin) = 0;


    virtual void addSystemInterruptHandler(InterruptHandler* handler, SystemInterrupt interrupt) = 0;
    */

    //Power management:

    /**
     * Powers down the microcontroller.
     */
    static void powerOff();
};

#endif
