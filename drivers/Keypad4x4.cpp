/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "Keypad4x4.h"


void Keypad4x4::configure(uint8_t option, uint8_t value)
{
    if (option == Keypad4x4::POLLING_MODE && value > 0) {
        this->polling_mode = true;
    }
}


char Keypad4x4::positionToChar(uint8_t row, uint8_t column)
{
    if (row == 0) {
        if (column == 0) {
            return '1';
        } else if (column == 1) {
            return '2';
        } else if (column == 2) {
            return '3';
        } else if (column == 3) {
            return 'A';
        }       
    } else if (row == 1) {
        if (column == 0) {
            return '4';
        } else if (column == 1) {
            return '5';
        } else if (column == 2) {
            return '6';
        } else if (column == 3) {
            return 'B';
        }
    } else if (row == 2) {
        if (column == 0) {
            return '7';
        } else if (column == 1) {
            return '8';
        } else if (column == 2) {
            return '9';
        } else if (column == 3) {
            return 'C';
        }
    } else if (row == 3) {
        if (column == 0) {
            return '*';
        } else if (column == 1) {
            return '0';
        } else if (column == 2) {
            return '#';
        } else if (column == 3) {
            return 'D';
        }
    }
    return '\0';
}


char Keypad4x4::readKey()
{
    char value = '\0';
    this->readKeys(1, &value);
    return value;
}


void Keypad4x4::readKeys(uint8_t max_amount, char* keys)
{
    uint8_t read_keys = 0;
    if (this->polling_mode) {
        for (uint8_t i = 0; i < 4; i++) {
            //Set the row pin high.
            Microcontroller::writePin(this->row_pins[i], true);
            for (uint8_t j = 0; j < 4; j++) {
                //Check if one of the column pins is high.
                if (Microcontroller::readPin(this->column_pins[j])) {
                    keys[read_keys] = this->positionToChar(i, j);
                    read_keys++;
                    //Check if the maximum amount of keys has been read:
                    if (read_keys >= max_amount) {
                        //Set the row pin low and return.
                        Microcontroller::writePin(this->row_pins[i], false);
                        return;
                    }
                }
            }
            //Before moving to the next row, the row pin of
            //the current row has to be set low.
            Microcontroller::writePin(this->row_pins[i], false);
        }
    }
}


void Keypad4x4::applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin)
{
    if (application_pin == APIN::D0) {
        this->row_pins[0] = microcontroller_pin;
    } else if (application_pin == APIN::D1) {
        this->row_pins[1] = microcontroller_pin;
    } else if (application_pin == APIN::D2) {
        this->row_pins[2] = microcontroller_pin;
    } else if (application_pin == APIN::D3) {
        this->row_pins[3] = microcontroller_pin;
    } else if (application_pin == APIN::D4) {
        this->column_pins[0] = microcontroller_pin;
    } else if (application_pin == APIN::D5) {
        this->column_pins[1] = microcontroller_pin;
    } else if (application_pin == APIN::D6) {
        this->column_pins[2] = microcontroller_pin;
    } else if (application_pin == APIN::D7) {
        this->column_pins[3] = microcontroller_pin;
    }

    if (this->polling_mode) {
        //Make the row pins outputs:
        Microcontroller::setPinDirection(this->row_pins[0], true);
        Microcontroller::setPinDirection(this->row_pins[1], true);
        Microcontroller::setPinDirection(this->row_pins[2], true);
        Microcontroller::setPinDirection(this->row_pins[3], true);
    }
}
