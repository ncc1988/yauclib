/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__ROTARYENCODER_H
#define YAUCLIB__ROTARYENCODER_H


#include "Driver.h"


class RotaryEncoder: public Driver
{
    protected:

    /**
     * The pin to use for receiving the clock signal.
     */
    uint8_t clock_pin = 0;

    /**
     * The pin to use for receiving the data signal (rotation direction).
     */
    uint8_t data_pin = 0;

    /**
     * A delay in miliseconds after taking a measurement.
     * This is for rotary encoders that bounce after a rotation and may
     * therefore produce invalid results if the next measurement is taken
     * too early.
     *
     * This defaults to 10 miliseconds.
     */
    uint8_t delay = 50;

    /**
     * Whether the result of the rotation measurement shall be inverted (true)
     * or not (false). Defaults to false.
     */
    bool inverted = false;

    /**
     * This attribute indicates whether valid data are in the attributes
     * measurement_rotating and measurement_clockwise. In case to measurement
     * has been taken, this attribute is false. As soon as the first measurement
     * has been taken, it is set to true.
     */
    bool valid_data = false;

    /**
     * This attribute stores whether a rotation did take place during the
     * last measurement (true) or not (false).
     * Rotation means that the clock signal of the rotary encoder is on a
     * logical high level.
     */
    bool measurement_rotated = false;

    /**
     * This attribute stores the rotation of the last measurement.
     * True means a clockwise rotation has been measured. False means a
     * counter-clockwise rotation.
     */
    bool measurement_clockwise = false;

    public:

    /**
     * @see Driver::applyPinMapping
     *
     * NOTE: This driver uses the application pins SCK (clock signal)
     * and SDA (data).
     */
    virtual void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin);

    /**
     * Inverts the result of the rotation measurement.
     */
    virtual void invertDirections();

    /**
     * Sets the delay after a measurement has been taken.
     *
     * @param uint8_t delay The delay to use before taking a measurement.
     */
    virtual void setDelay(uint8_t delay);

    /**
     * Takes a measurement of the rotation.
     */
    virtual void takeMeasurement();

    /**
     * This method indicates whether valid data have been stored.
     *
     * @returns bool True, if valid data are present, false otherwise.
     */
    virtual bool hasValidData();

    /**
     * This method returns the status of the measurement_rotated attribute.
     *
     * @returns True, if the measurement indicated rotation, false otherwise.
     */
    virtual bool hasRotated();

    /**
     * This method returns the status of the measurement_clockwise attribute.
     *
     * @returns bool True, if the measurement indicated clockwise rotation,
     *     false otherwise.
     */
    virtual bool turnedClockwise();
};


#endif
