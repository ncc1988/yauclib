/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__APIN_H
#define YAUCLIB__APIN_H


/**
 * Application pin code definitions.
 *
 * Codes from 201 to 255 are application specific and will not be assigned.
 */
enum APIN
{
    /**
     * A general purpose pin.
     */
    GENERAL = 0,

    /**
     * Serial data
     */
    SDA = 1,

    /**
     * Serial clock
     */
    SCK = 2,

    /**
     * Enable
     */
    ENABLE = 3,

    /**
     * Disable
     */
    DISABLE = 4,

    /**
     * A read indicator pin..
     */
    READ = 10,

    /**
     * A write indicator pin.
     */
    WRITE = 11,

    /**
     * Parallel data pin 0
     */
    D0 = 20,

    /**
     * Parallel data pin 1
     */
    D1 = 21,

    /**
     * Parallel data pin 2
     */
    D2 = 22,

    /**
     * Parallel data pin 3
     */
    D3 = 23,

    /**
     * Parallel data pin 4
     */
    D4 = 24,

    /**
     * Parallel data pin 5
     */
    D5 = 25,

    /**
     * Parallel data pin 6
     */
    D6 = 26,

    /**
     * Parallel data pin 7
     */
    D7 = 27,

    /**
     * Parallel data pin 8
     */
    D8 = 28,

    /**
     * Parallel data pin 9
     */
    D9 = 29,

    /**
     * Parallel data pin 10
     */
    D10 = 30,

    /**
     * Parallel data pin 11
     */
    D11 = 31,

    /**
     * Parallel data pin 12
     */
    D12 = 32,

    /**
     * Parallel data pin 13
     */
    D13 = 33,

    /**
     * Parallel data pin 14
     */
    D14 = 34,

    /**
     * Parallel data pin 15
     */
    D15 = 35
};


#endif
