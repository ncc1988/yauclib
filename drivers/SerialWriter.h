/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__SERIALWRITER_H
#define YAUCLIB__SERIALWRITER_H


#include "StreamWriter.h"


class SerialWriter: public StreamWriter
{
    protected:


    /**
     * The serial clock pin, commonly referred to as SCK.
     */
    uint8_t serial_clock_pin;


    /**
     * The serial data pin, commonly referred to as SDA.
     */
    uint8_t serial_data_pin;


    public:


    /**
     * @see Driver::applyPinMapping
     */
    virtual void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin) override;


    /**
     * @see StreamWriter::write
     */
    virtual void write(const uint8_t byte) override;
};


#endif
