/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2022 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "EONE1602.h"


void EONE1602::applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin)
{
    if (application_pin == APIN::ENABLE) {
        this->enable_pin = microcontroller_pin;
        Microcontroller::setPinDirection(this->enable_pin, true);
    } else if (application_pin == APIN::READ) {
        this->rw_pin = microcontroller_pin;
        Microcontroller::setPinDirection(this->rw_pin, true);
    } else if (application_pin == APIN::D0) {
        this->data_pins[0] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[0], true);
    } else if (application_pin == APIN::D1) {
        this->data_pins[1] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[1], true);
    } else if (application_pin == APIN::D2) {
        this->data_pins[2] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[2], true);
    } else if (application_pin == APIN::D3) {
        this->data_pins[3] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[3], true);
    } else if (application_pin == APIN::D4) {
        this->data_pins[4] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[4], true);
    } else if (application_pin == APIN::D5) {
        this->data_pins[5] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[5], true);
    } else if (application_pin == APIN::D6) {
        this->data_pins[6] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[6], true);
    } else if (application_pin == APIN::D7) {
        this->data_pins[7] = microcontroller_pin;
        Microcontroller::setPinDirection(this->data_pins[7], true);
    }
}


void EONE1602::setRsPin(uint8_t microcontroller_pin)
{
    this->rs_pin = microcontroller_pin;
    Microcontroller::setPinDirection(this->rs_pin, true);
}


void EONE1602::setBacklightPin(uint8_t microcontroller_pin)
{
    this->backlight_pin = microcontroller_pin;
    Microcontroller::setPinDirection(this->backlight_pin, true);
}


void EONE1602::writeData(uint8_t byte)
{
    if (this->four_bit_mode) {
        //In four bit mode, we only send the lowest 4 bits
        //over the highest 4 pins (DB4 - DB7).
        Microcontroller::writePin(this->data_pins[4], (byte & 0x01) > 0);
        Microcontroller::writePin(this->data_pins[5], (byte & 0x02) > 0);
        Microcontroller::writePin(this->data_pins[6], (byte & 0x04) > 0);
        Microcontroller::writePin(this->data_pins[7], (byte & 0x08) > 0);
    } else {
        //Eight bit mode: send the whole byte.
        Microcontroller::writePin(this->data_pins[0], (byte & 0x01) > 0);
        Microcontroller::writePin(this->data_pins[1], (byte & 0x02) > 0);
        Microcontroller::writePin(this->data_pins[2], (byte & 0x04) > 0);
        Microcontroller::writePin(this->data_pins[3], (byte & 0x08) > 0);
        Microcontroller::writePin(this->data_pins[4], (byte & 0x10) > 0);
        Microcontroller::writePin(this->data_pins[5], (byte & 0x20) > 0);
        Microcontroller::writePin(this->data_pins[6], (byte & 0x40) > 0);
        Microcontroller::writePin(this->data_pins[7], (byte & 0x80) > 0);
    }
}


void EONE1602::waitUntilReady()
{
    Microcontroller::setPinDirection(this->data_pins[7], false);
    Microcontroller::writePin(this->rs_pin, 0);
    Microcontroller::writePin(this->rw_pin, 1);
    while (!Microcontroller::readPin(this->data_pins[7])) {
        Microcontroller::waitMicroseconds(1);
    }
    Microcontroller::writePin(this->rw_pin, 0);
    Microcontroller::setPinDirection(this->data_pins[7], true);
}


void EONE1602::sendCommand(uint8_t command)
{
    if (this->four_bit_mode) {
        this->waitUntilReady();
        //When waitUntilReady has returned, it has already
        //set the rs and rw pins low.
        //Send the four highest bits first, but shift them
        //to the lower four bits before that:
        this->writeData(command >> 4);
        Microcontroller::writePin(this->enable_pin, 1);
        Microcontroller::wait(1);
        Microcontroller::writePin(this->enable_pin, 0);
        this->waitUntilReady();
        //Now send the lowest four bits:
        this->writeData(command);
        Microcontroller::writePin(this->enable_pin, 1);
        Microcontroller::wait(1);
        Microcontroller::writePin(this->enable_pin, 0);
        this->waitUntilReady();
    } else {
        this->waitUntilReady();
        //When waitUntilReady has returned, it has already
        //set the rs and rw pins low.
        this->writeData(command);
        Microcontroller::writePin(this->enable_pin, 1);
        Microcontroller::wait(1);
        Microcontroller::writePin(this->enable_pin, 0);
        this->waitUntilReady();
    }
}


void EONE1602::initConnection(bool four_bit_mode)
{
    uint8_t command = 0x30;
    if (four_bit_mode) {
        command = 0x20;
    }
    this->sendCommand(command);
    //The four_bit_mode attribute needs to be set after
    //the command has been set, otherwise the bit shifting
    //in setCommand will already be applied.
    this->four_bit_mode = four_bit_mode;
}


void EONE1602::configure(
    bool two_line_mode,
    bool big_font,
    bool rtl_mode,
    bool shift_display,
    bool cursor_enabled,
    bool cursor_blinking_enabled
    )
{
    //Clear display:
    this->sendCommand(0x01);

    //Reset the cursor (return home):
    this->sendCommand(0x02);

    //Basic display configuration:

    uint8_t command = 0x30;
    if (this->four_bit_mode) {
        command = 0x20;
    }
    if (two_line_mode) {
        command |= 0x08;
    }
    if (big_font) {
        command |= 0x04;
    }
    this->sendCommand(command);
    this->two_line_mode = two_line_mode;

    command = 0x00;

    //rtl/ltr mode and display shift configuration:

    if (rtl_mode) {
        command = 0x04;
    } else {
        command = 0x06;
    }
    if (shift_display) {
        command |= 0x01;
    }
    this->sendCommand(command);
    this->rtl_mode = rtl_mode;

    //Cursor configuration:

    command = 0x0C;
    if (cursor_enabled) {
        command |= 0x02;
    }
    if (cursor_blinking_enabled) {
        command |= 0x01;
    }
    this->sendCommand(command);
}


void EONE1602::clear()
{
    this->sendCommand(0x01);
}


void EONE1602::resetCursor()
{
    this->sendCommand(0x02);
}


void EONE1602::setRtl(bool shift_entire_display)
{
    if (shift_entire_display) {
        this->sendCommand(0x05);
    } else {
        this->sendCommand(0x04);
    }
    this->rtl_mode = true;
}


void EONE1602::setLtr(bool shift_entire_display)
{
    if (shift_entire_display) {
        this->sendCommand(0x07);
    } else {
        this->sendCommand(0x06);
    }
    this->rtl_mode = false;
}


void EONE1602::switchOn()
{
    this->sendCommand(0x0C);
}


void EONE1602::switchOff()
{
    this->sendCommand(0x08);
}


void EONE1602::enableCursor(bool blink)
{
    if (blink) {
        this->sendCommand(0x0F);
    } else {
        this->sendCommand(0x0E);
    }
}


void EONE1602::disableCursor()
{
    this->sendCommand(0x0C);
}


void EONE1602::shiftCursor(bool left, bool with_display)
{
    if (with_display) {
        if (left) {
            this->sendCommand(0x18);
        } else {
            this->sendCommand(0x1C);
        }
    } else {
        if (left) {
            this->sendCommand(0x10);
        } else {
            this->sendCommand(0x14);
        }
    }
}


void EONE1602::setPosition(uint8_t pos)
{
    pos |= 0x80;

    this->sendCommand(pos);
}


void EONE1602::backlightOn()
{
    Microcontroller::writePin(this->backlight_pin, true);
}


void EONE1602::backlightOff()
{
    Microcontroller::writePin(this->backlight_pin, false);
}


void EONE1602::backlightLevel(uint8_t level)
{
    Microcontroller::writeAnalogPin(this->backlight_pin, level);
}


void EONE1602::printChar(const uint8_t character)
{
    if (this->four_bit_mode) {
        //Send the four highest bits first, but shift them
        //to the lower four bits before that:
        Microcontroller::writePin(this->rs_pin, 1);
        Microcontroller::writePin(this->rw_pin, 0);
        this->writeData(character >> 4);
        Microcontroller::writePin(this->enable_pin, 1);
        Microcontroller::writePin(this->enable_pin, 0);
        //Now send the lowest four bits:
        Microcontroller::writePin(this->rs_pin, 1);
        Microcontroller::writePin(this->rw_pin, 0);
        this->writeData(character);
        Microcontroller::writePin(this->enable_pin, 1);
        Microcontroller::writePin(this->enable_pin, 0);
    } else {
        Microcontroller::writePin(this->rs_pin, 1);
        Microcontroller::writePin(this->rw_pin, 0);
        this->writeData(character);
        Microcontroller::writePin(this->enable_pin, 1);
        Microcontroller::writePin(this->enable_pin, 0);
    }
}


void EONE1602::print(const char* text)
{
    this->waitUntilReady();
    while (*text != 0) {
        this->printChar(*text);
        text++;
    }
}


void EONE1602::printAtPosition(const uint8_t line, const uint8_t column, const char* text)
{
    this->moveToPosition(line, column);
    this->print(text);
}


void EONE1602::moveToPosition(const uint8_t line, const uint8_t column)
{
    uint8_t start_addr = 0x00;
    if (this->two_line_mode) {
        if (line > 0) {
            //The second line starts on address 0x40.
            if (this->rtl_mode) {
                start_addr = 0x67;
            } else {
                start_addr = 0x40;
            }
        } else {
            if (this->rtl_mode) {
                //The first line starts on 0x27.
                start_addr = 0x27;
            }
            //No else here: For the first line in ltr mode,
            //the start address 0x00 is correct.
        }
    } else {
        //One line mode.
        if (this->rtl_mode) {
            start_addr = 0x4F;
        }
        //No else here either, for the same reason: 0x00 is the correct
        //address for ltr mode.
    }
    if (this->rtl_mode) {
        start_addr -= column;
    } else {
        start_addr += column;
    }
    this->setPosition(start_addr);
}


void EONE1602::printShort(const uint16_t number, const uint8_t base)
{
    this->waitUntilReady();
    if (base > 16) {
        //We currently cannot deal with numbers that have such a big base.
        this->printChar('x');
        return;
    }

    if (number == 0) {
        //This is easy:
        this->printChar('0');
        return;
    }

    uint16_t remainder = number;
    uint32_t current_modulo = base;
    //Start with the most significant digit and then go lower
    //until there is no space left.
    while (current_modulo <= UINT16_MAX) {
        current_modulo *= base;
    }
    //Reduce current_modulo by one step to stay in the range
    //of uint16_t integers:
    current_modulo /= base;

    uint8_t current_digit = 0;
    bool nonzero_digit = false;
    do {
        current_digit = remainder / current_modulo;
        remainder = remainder % current_modulo;
        current_modulo /= base;

        if ((current_digit == 0) && !nonzero_digit) {
            //The current digit is zero and there haven't been any
            //non-zero digits before that. We can skip printing
            //leading zeros.
            continue;
        } else if (current_digit > 0) {
            nonzero_digit = true;
        }

        if (current_digit > 10) {
            //Use hex numbers A-F
            current_digit += 0x37;
        } else {
            //Use the numbers 0-9:
            current_digit += 0x30;
        }
        this->printChar(current_digit);
    } while (current_modulo > 0);
}
