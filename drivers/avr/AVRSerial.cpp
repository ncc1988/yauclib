/**
 * This file is part of the yauclib library.
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "AVRSerial.h"


void AVRSerial::applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin)
{
    //Nothing to do.
}


void AVRSerial::write(const uint8_t byte)
{
    if (!this->configured) {
        return;
    }

    //Wait until ready:
    while (!(UCSR0A & (1 << UDRE0)));
    //Set the byte to be sent:
    UDR0 = byte;
}

uint8_t AVRSerial::read()
{
    if (!this->configured) {
        return 0;
    }

    //Wait until data are ready:
    while (!(UCSR0A & (1 << RXC0)));
    return UDR0;
}


void AVRSerial::configureInterface(uint32_t baud_rate, bool use_parity_bit, bool use_even_parity)
{
    uint16_t ubrr_value = ((Microcontroller::clock_speed * 1000) / (16 * baud_rate)) - 1;
    if (ubrr_value > 0x0fff) {
        //Value too big.
        return;
    }
    UBRR0H = ubrr_value >> 8;
    UBRR0L = ubrr_value & 0xff;

    //Default to 8 bit character size and asynchronous mode:
    UCSR0C = (3 << UCSZ00);;

    if (use_parity_bit) {
        uint8_t parity_bits_value = 0;
        //Use a parity bit.
        if (use_even_parity) {
            //Use even parity.
            parity_bits_value = 2;
        } else {
            //Use odd parity.
            parity_bits_value = 3;
        }
        UCSR0C |= (parity_bits_value << UPM00);
    }
    //Enable the transmitter:
    UCSR0B |= 1 << TXEN0;
    //Enable the receiver:
    UCSR0B |= 1 << RXEN0;

    //The configuration is complete:
    this->configured = true;
}


uint16_t AVRSerial::writeString(const char* str)
{
    if (!this->configured) {
        return 0;
    }
    const char* str_ptr = str;
    uint16_t c = 0;
    while (*str_ptr != 0) {
        //Write the byte as soon as the transmitter is ready:
        this->write(*str_ptr);
        str_ptr++;
        c++;
    }
    return c;
}
