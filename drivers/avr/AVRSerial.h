/**
 * This file is part of the yauclib library.
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__DRIVERS__AVRSERIAL_H
#define YAUCLIB__DRIVERS__AVRSERIAL_H


#if defined UC_FAMILY_AVR || UC_FAMILY_ARDUINO


#include <avr/io.h>


#include "../Driver.h"
#include "../StreamWriter.h"


/**
 * The AVRSerial driver uses the integrated serial interface hardware
 * of AVR microcontrollers. It is therefore only available on the AVR/Arduino
 * microcontroller family.
 */
class AVRSerial: public StreamWriter /*, StreamReader*/
{
    protected:

    /**
     * A status flag indicating whether the serial interface has been
     * configured (true) or not (false).
     */
    bool configured = false;

    public:

    /**
     * @see Driver::applyPinMapping
     *
     * NOTE: This method is intentionally left empty because the hardware pins
     * are fixed.
     */
    virtual void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin);

    /**
     * @see StreamWriter::write
     */
    virtual void write(const uint8_t byte);

    //Not yet standardised methods:

    virtual uint8_t read();

    //Custom methods:

    /**
     * Configures the serial interface.
     *
     * @param uint32_t baud_rate The baud rate to use.
     *
     * @param bool use_parity_bit Whether to use the parity bit (true)
     *     or not (false)
     *
     * @param bool use_even_parity Whether to use even parity (true)
     *     or odd parity (false). Only regarded when use_parity_bit
     *     is set to true.
     */
    virtual void configureInterface(uint32_t baud_rate, bool use_parity_bit, bool use_even_parity);

    /**
     * Writes a null-terminated string.
     *
     * IMPORTANT: The string must be null-terminated since encountering a
     * character with the value 0 (0x00 or "\0") is the condition for
     * stopping the writing process.
     *
     * @param char* str The null-terminated string to write.
     *
     * @returns uint16_t The amount of written characters.
     */
    virtual uint16_t writeString(const char* str);
};


#endif


#endif
