/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "SerialWriter.h"


void SerialWriter::applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin)
{
    if (application_pin == APIN::SCK) {
        this->serial_clock_pin = microcontroller_pin;
        Microcontroller::setPinDirection(this->serial_clock_pin, true);
    } else if (application_pin == APIN::SDA) {
        this->serial_data_pin = microcontroller_pin;
        Microcontroller::setPinDirection(this->serial_data_pin, true);
    }
}


void SerialWriter::write(const uint8_t byte)
{
    //Set the clock pin to low:
    Microcontroller::writePin(this->serial_clock_pin, false);

    //Read each bit and send it via the data pin, MSB first.
    for (uint8_t i = 0; i < 8; i++) {
        Microcontroller::writePin(
            this->serial_data_pin,
            ((byte << i) & 0x80) > 0
            );
        //Set the serial clock pin high, wait and then set it low:
        Microcontroller::writePin(this->serial_clock_pin, true);
        Microcontroller::waitMicroseconds(10);
        Microcontroller::writePin(this->serial_clock_pin, false);
        Microcontroller::waitMicroseconds(10);
    }

    //Set the data pin to low:
    Microcontroller::writePin(this->serial_data_pin, false);
}
