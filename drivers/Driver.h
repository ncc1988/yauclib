/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__DRIVER_H
#define YAUCLIB__DRIVER_H


#include <inttypes.h>


#include "APIN.h"
#include "../Microcontroller.h"



/**
 * The Driver class is the base class for all hardware drivers.
 */
class Driver
{
    public:


    /**
     * Maps application pins to microcontroller pins. This means that pin names
     * like SCL, SDA, D0, D1 etc. are mapped to the pin names of the
     * microcontroller.
     *
     * @param uint8_t application_pin The application pin number
     *     (see APIN.h for pin number aliases).
     *
     * @param uint8_t microcontroller_pin The microcontroller pin.
     */
    virtual void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin) = 0;
};


#endif
