/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2022 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__EONE1602_H
#define YAUCLIB__EONE1602_H


#include "Driver.h"


class EONE1602: public Driver
{
    protected:


    uint8_t rs_pin = 0;


    uint8_t rw_pin = 0;


    uint8_t enable_pin = 0;


    uint8_t data_pins[8] = {0, 0, 0, 0, 0, 0, 0, 0};


    uint8_t backlight_pin = 0;


    /**
     * Four bit data transfer mode (true) or eight bit mode (false).
     */
    bool four_bit_mode = false;


    /**
     * Two line mode enabled (true) or not (false).
     */
    bool two_line_mode = false;


    /**
     * Right to left writing mode enabled (true) or not (false).
     */
    bool rtl_mode = false;


    public:


    /**
     * @see Driver::applyPinMapping
     */
    virtual void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin);


    /**
     * The RS pin is application specific and gets its own setter.
     */
    void setRsPin(uint8_t microcontroller_pin);


    /**
     * The backlight pin is application specific and gets its own setter.
     */
    void setBacklightPin(uint8_t microcontroller_pin);


    /**
     * Writes a byte to the 8 data pins.
     */
    void writeData(uint8_t byte);


    /**
     * Waits until the display clears the busy flag (DB7 when RW is high).
     */
    void waitUntilReady();


    /**
     * Sends a command to the display.
     */
    void sendCommand(uint8_t command);


    /**
     * Initialises the data connection to the display to set either the
     * four bit data mode or the eight bit data mode.
     *
     * @param bool four_bit_mode Whether to use the four bit mode (true)
     *     or to use the eight bit data transfer mode (false).
     *     Defaults to false.
     */
    void initConnection(bool four_bit_mode = false);


    /**
     * Configures the display features all in one.
     *
     * @param bool two_line_mode Whether to enable two line mode (true)
     *     or to use one line (false). Defaults to false.
     *
     * @param bool big_font Whether to use the big 5x11 font (true)
     *     or to use the 5x8 font (false). Defaults to false.
     *
     * @param bool rtl_mode Whether to use right to left (rtl) text
     *     mode (true) or left to right (false). Defaults to false.
     *
     * @param bool shift_display Whether the display shall be shifted
     *     (true) or not (false). Defaults to false.
     *     TODO: better description!
     *
     * @param bool cursor_enabled Whether to enable the cursor (true)
     *     or not (false). Defaults to false.
     *
     * @param bool cursor_blinking_enabled Whether to enable a blinking cursor
     *     (true) or not (false). Defaults to false.
     */
    void configure(
        bool two_line_mode = false,
        bool big_font = false,
        bool rtl_mode = false,
        bool shift_display = false,
        bool cursor_enabled = false,
        bool cursor_blinking_enabled = false
        );



    /**
     * Clears the display.
     */
    void clear();


    /**
     * Resets the cursor.
     */
    void resetCursor();


    /**
     * Sets right to left writing order.
     *
     * @param bool shift_entire_display TODO
     */
    void setRtl(bool shift_entire_display = false);


    /**
     * Sets left to right writing order.
     *
     * @param bool shift_entire_display TODO
     */
    void setLtr(bool shift_entire_display = false);


    /**
     * Switches the display on.
     */
    void switchOn();


    /**
     * Switches the display off.
     */
    void switchOff();


    /**
     * Enables the cursor on the display.
     *
     * @param bool blink Whether the cursor shall blink (true)
     *     or not (false). Defaults to false.
     */
    void enableCursor(bool blink = false);


    /**
     * Disables the cursor.
     */
    void disableCursor();


    /**
     * Shifts the cursor.
     *
     * @param bool left Whether to shift the cursor to the left (true)
     *     or to the right (false). Defaults to false.
     *
     * @param bool with_display TODO
     */
    void shiftCursor(bool left = false, bool with_display = false);


    /**
     * Sets the position for the next character.
     */
    void setPosition(uint8_t pos = 0);


    /**
     * Switches the display backlight on.
     */
    void backlightOn();


    /**
     * Switches the display backlight off.
     */
    void backlightOff();


    /**
     * Sets the backlight brightness level.
     * Note that this only works when the pin of the backlight
     * LED is connected to a pin that can produce a PWM.
     */
    void backlightLevel(uint8_t level);


    /**
     * Prints one character on the display.
     *
     * @param uint8_t character The character to print.
     */
    void printChar(uint8_t character);


    /**
     * Prints a null-terminated string at the current cursor position.
     *
     * Note: This method will only stop printing string characters
     * until it finds a NUL character ('\0'). Until it reaches that
     * point, it just reads the next memory address.
     *
     * @param const char* text The text to be printed.
     */
    void print(const char* text);


    /**
     * Prints a null-terminated string on the display at a specific position.
     *
     * Note: This method will only stop printing string characters
     * until it finds a NUL character ('\0'). Until it reaches that
     * point, it just reads the next memory address.
     *
     * @param const char* text The text to be printed.
     */
    [[deprecated]] void printAtPosition(uint8_t line, uint8_t column, const char* text);


    /**
     * Moves the cursor to a certain position.
     *
     * @param uint8_t line The line number starting at 0.
     *
     * @param uint8_t column The column number starting at 0.
     *
     */
    void moveToPosition(const uint8_t line, const uint8_t column);


    /**
     * Prints a short (16-bit) integer at the current cursor position.
     *
     * @param const uint16_t number The number to be printed.
     */
    void printShort(const uint16_t number, const uint8_t base = 10);
};


#endif
