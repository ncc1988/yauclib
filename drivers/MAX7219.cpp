/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "MAX7219.h"


void MAX7219::applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin)
{
    if (application_pin == APIN::ENABLE) {
        this->enable_pin = microcontroller_pin;
        Microcontroller::setPinDirection(this->enable_pin, true);
    } else {
        SerialWriter::applyPinMapping(application_pin, microcontroller_pin);
    }
}


void MAX7219::scrollText(const uint8_t* data, const uint8_t size)
{
    if (data == nullptr) {
        return;
    }

    //We must use counter variables here, since we are
    //printing a range of the provided data
    //so that the value of 8 bytes are visible at once.
    for (uint8_t i = 0; i < size; i++) {
        uint8_t segment = 1;
        this->clear();
        for (uint8_t j = i; ((j < i + 8) && (j < size)); j++) {
            this->setSegment(segment, data[j]);
            segment++;
        }
    }
}


void MAX7219::enable()
{
    this->writeRegister(0x0C, 0x01);
}


void MAX7219::disable()
{
    this->writeRegister(0x0C, 0x00);
}


void MAX7219::setEnabledDigitAmount(uint8_t digits)
{
    this->writeRegister(0x0B, (--digits) & 0x07);
}



void MAX7219::clear()
{
    for (uint8_t i = 1; i < 9; i++) {
        this->setSegment(i, 0);
    }
}


void MAX7219::setSegment(uint8_t segment, uint8_t value)
{
    if ((segment == 0) || (segment > 8)) {
        //These are not segment addresses!
        return;
    }
    this->writeRegister(segment, value);
}


void MAX7219::enableBcdMode()
{
    this->writeRegister(0x9, 0xFF);
}


void MAX7219::disableBcdMode()
{
    this->writeRegister(0x9, 0x00);
}


void MAX7219::setBcdMode(uint8_t segment_mode)
{
    this->writeRegister(0x9, segment_mode);
}


void MAX7219::setBrightness(uint8_t value)
{
    //The brightness has 16 steps, so we have to
    //divide the value by 16 to get into the valid
    //brightness range.
    uint8_t real_brightness = value / 16;
    this->writeRegister(0xA, real_brightness);
}


void MAX7219::writeRegister(uint8_t reg, uint8_t value)
{
    //Clear the four upper bits of reg, since they are not used:
    reg &= 0x0F;

    //Make sure to create a falling edge on the enable pin
    //before sending any data:
    if (!Microcontroller::readPin(this->enable_pin)) {
        Microcontroller::writePin(this->enable_pin, true);
        Microcontroller::waitMicroseconds(10);
    }
    Microcontroller::writePin(this->enable_pin, false);

    //Write the 16 bit of data: 4 bit zero, then the 4 least
    //significant bits of reg, then the value:
    SerialWriter::write(reg);
    SerialWriter::write(value);

    /*
    Microcontroller::writePin(this->serial_data_pin, false);
    for (uint8_t i = 0; i < 16; i++) {
        if (i > 3) {
            //The four unused bits have been sent.
            if (i > 7) {
                //Send the value.
                //Only the last 3 bits of i are used, so that the
                //value is shifted 0 to 8 bits.
                Microcontroller::writePin(
                    this->serial_data_pin,
                    (value << (i & 0x07)) & 0x80
                    );
            } else {
                //Send the register number. Shift the value of
                //reg i bits. The current bit is the MSB.
                Microcontroller::writePin(
                    this->serial_data_pin,
                    (reg << i) & 0x80
                    );
            }
        }
        //Set the serial clock pin high, wait and then set it low:
        Microcontroller::writePin(this->serial_clock_pin, true);
        Microcontroller::waitMicroseconds(10);
        Microcontroller::writePin(this->serial_clock_pin, false);
        Microcontroller::waitMicroseconds(10);
    }
    //Clear the data pin:
    Microcontroller::writePin(this->serial_data_pin, false);
    */
    //Enable the MAX7219 again:
    Microcontroller::writePin(this->enable_pin, true);
}
