/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__LED_H
#define YAUCLIB__LED_H


#include "Driver.h"
//#include "../TimerInterruptHandler.h"



class LED: public Driver /*, TimerInterruptHandler */
{
    protected:


    uint8_t pin = 0;


    public:


    /**
     * @see Driver::applyPinMapping
     *
     * Sets the pin where the LED is connected to.
     *
     * @param uint8_t application_pin The application pin.
     *     Only APIN::GENERAL is supported!
     *
     * @param uint8_t The microcontroller pin number
     *     where the LED is connected to.
     */
    void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin);


    /**
     * Sets the brightness of the LED. Note that this only works on pins
     * that can output a PWM. Pins that can't do that or are not configured
     * for that will just let the LED shine with full brightness, if the
     * value of brightness is greater than zero.
     *
     * @param uint8_t brightness The brightness of the LED.
     *     0 means off, 255 means full brightness.
     */
    void setBrightness(uint8_t brightness);
};


#endif
