/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "RotaryEncoder.h"



void RotaryEncoder::applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin)
{
    if (application_pin == APIN::SCK) {
        this->clock_pin = microcontroller_pin;
    }
    if (application_pin == APIN::SDA) {
        this->data_pin = microcontroller_pin;
    }
}


void RotaryEncoder::invertDirections()
{
    this->inverted = !this->inverted;
}


void RotaryEncoder::setDelay(uint8_t delay)
{
    this->delay = delay;
}


void RotaryEncoder::takeMeasurement()
{
    if (this->clock_pin == 0 || this->data_pin == 0) {
        //Invalid pin mapping.
        return;
    }

    this->measurement_rotated = Microcontroller::readPin(this->clock_pin);
    if (this->measurement_rotated) {
        //Check in which direction the rotation did take place:
        if (this->inverted) {
            this->measurement_clockwise = !Microcontroller::readPin(this->data_pin);
        } else {
            this->measurement_clockwise = Microcontroller::readPin(this->data_pin);
        }
    }
    this->valid_data = true;
    if (this->delay) {
        Microcontroller::wait(this->delay);
    }
}


bool RotaryEncoder::hasValidData()
{
    return this->valid_data;
}


bool RotaryEncoder::hasRotated()
{
    return this->measurement_rotated;
}


bool RotaryEncoder::turnedClockwise()
{
    return this->measurement_clockwise;
}
