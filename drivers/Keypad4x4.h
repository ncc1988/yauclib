/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2024 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "Driver.h"


/**
 * This is the driver for a 4 row 4 column keypad that is used
 * in Arduino environments.
 */
class Keypad4x4: public Driver
{
    protected:

    /**
     * The pins for the 4 rows.
     *
     * In polling mode, the driver uses these as outputs and
     * sets them to a high level.
     */
    uint8_t row_pins[4] = {0,0,0,0};

    /**
     * The pins for the 4 columns. The driver only reads from
     * those pins and checks whether they have a high or low
     * level.
     */
    uint8_t column_pins[4] = {0,0,0,0};

    /**
     * Whether to use polling to get the pressed key (true)
     * or not (false). Defaults to false.
     */
    bool polling_mode = false;

    /**
     * Maps a row and column position to an ASCII character.
     *
     * @param uint8_t row The row number, starting from 0.
     *
     * @param uint8_t column The column number, starting from 0.
     *
     * @returns The ASCII character for that position or \0 in case
     *     the position cannot be mapped to a character.
     */
    char positionToChar(uint8_t row, uint8_t column);
    
    public:

    enum {
        /**
         * POLLING_MODE tells the driver to set all row lines
         * to a high level one by one and then check the values
         * of the column lines to get the pressed keys.
         */
        POLLING_MODE = 0
    };

    /**
     * Configures the driver.
     *
     * NOTE: This method should be called before applyPinMapping
     * so that the row pins can be set as outputs when polling
     * mode is used.
     *
     * NOTE: This is a prototype for a generic configuration method
     * that could be transferred to the driver class.
     *
     * @param uint8_t option The option to set.
     *
     * @param uint8_t value The value to set for the option.
     *     For boolean options, all values greater zero are
     *     considered true.
     */
    void configure(uint8_t option, uint8_t value);

    /**
     * Reads the pressed key as ASCII character.
     *
     * If multiple keys are pressed, only the first pressed
     * one is returned.
     *
     * If no key is pressed, the value 0 (\0) is returned.
     *
     * @returns char The ASCII character code of the pressed key.
     *     This can be the characters 0 - 9, "*", "#" and A - D when
     *     a key is pressed. If no key is pressed, \0 is returned.
     */
    char readKey();

    /**
     * Reads multiple pressed keys at once and sets their ASCII
     * character values in an array of char. Note that the array
     * must have been initialised to hold enough space for the
     * maximum amount of keys to read.
     *
     * @see Keypad4x4::readKey for a description of the ASCII codes
     *     that may be read.
     *
     * @param uint8_t max_amount The maximum amount of pressed keys
     *     that shall be read. The hardware maximum is 16 keys.
     *
     * @param char* keys The array of char to store the pressed
     *     key values. If this shall be used as a a null-terminated
     *     string, $max_amount + 1 bytes must be reserved.
     */
    void readKeys(uint8_t max_amount, char* keys);

    /**
     * @see Driver::applyPinMapping
     *
     * This drivers use the pins D0 - D3 for the row pins and
     * D4 - D7 for the column pins.
     */
    virtual void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin);
};
