/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021-2023 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "LED.h"


void LED::applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin)
{
    if (application_pin == APIN::GENERAL) {
        this->pin = microcontroller_pin;
    }
    Microcontroller::setPinDirection(this->pin, true);
}


void LED::setBrightness(uint8_t brightness)
{
    //PWM brightness isn't supported at the moment:
    //Every brightness greater than zero is turned into
    //full power:
    Microcontroller::writePin(this->pin, brightness > 0);
}
