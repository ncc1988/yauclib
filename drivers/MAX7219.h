/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#ifndef YAUCLIB__MAX7219_H
#define YAUCLIB__MAX7219_H


#include "SerialWriter.h"


/**
 * This is a driver for the Maxim MAX7219 7-segment display driver.
 */
class MAX7219: public SerialWriter
{
    protected:


    uint8_t enable_pin;


    public:


    /**
     * @see Driver::applyPinMapping
     */
    virtual void applyPinMapping(uint8_t application_pin, uint8_t microcontroller_pin) override;


    /**
     * Display text and scrolls it.
     */
    virtual void scrollText(const uint8_t* data, const uint8_t size);


    /**
     * Enables the MAX7219 (leaves shutdown mode).
     */
    void enable();


    /**
     * Disables the MAX7219 (enters shutdown mode).
     */
    void disable();



    void setEnabledDigitAmount(uint8_t digits);


    /**
     * Clears (blanks) all segments.
     */
    void clear();


    /**
     * Sets one segment to the specified value.
     */
    void setSegment(uint8_t segment, uint8_t value);


    /**
     * Enables the BCD mode for all the segments.
     */
    void enableBcdMode();


    /**
     * Disables the BCD mode for all the segments.
     */
    void disableBcdMode();


    /**
     * Enables or disables the BCD mode for the segments,
     * depending on the bit value (1 = enable, 0 = disable).
     *
     * @param uint8_t segment_mode Each bit defines the BCD mode for
     *      a segment.
     */
    void setBcdMode(uint8_t segment_mode);


    /**
     * Sets the brightness of all segments.
     */
    void setBrightness(uint8_t value);


    /**
     * Writes data directly to one of the registers of the MAX7219.
     */
    void writeRegister(uint8_t reg, uint8_t value);
};


#endif
