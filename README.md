# yauclib - yet another microcontroller library

This library is an attempt to write device drivers and an abstraction layer
for different microcontroller families in C++.

## How it works

The microcontroller is represented using the Microcontroller class which
provides fundamental methods like writing and reading pin values. The
class acts as an abstraction layer so that programs do not need to include
mircocontroller-specific code.

Pin numbers represent the physical pins on the microcontrollers package instead
of using the microcontroller-specific port names. This code snippet illustrates
how microcontroller programs can be written using yauclib:

    //Setting Pin 5 high:
    Microcontroller::writePin(5, true);
    //Read from Pin 4:
    bool enabled = Microcontroller::readPin(4);

Here is an example, how you would make an LED blink using yauclib:

    //blink the LED: turn it on and off every 500 miliseconds:
    while (true) {
        //Set pin 3 high:
        Microcontroller::writePin(3, true);
        //Wait 500 ms:
        Microcontroller::wait(500);
        //Set pin 3 low:
        Microcontroller::writePin(3, false);
        //Wait 500 ms:
        Microcontroller::wait(500);
    }

In a similar way, you would access timer and PWM hardware of your microcontroller.

## Drivers

Each driver is placed in the "drivers" folder. They are only added to your
program if you include them using #include in your C++ program source file
and add the driver source file in the compilation process.

Each driver is derived from the "Driver" base class so that they have a common
interface for basic methods like mapping pins. Drivers may also include special
methods if they are needed to use the device they are driving
in a certain manner.

To faciliate the mapping between device and microcontroller pins, each driver
has a set of named so-called "application pins". These are then mapped to
physical microcontroller pin numbers using the "applyPinMapping" method.
With this mapping, you can simply write which application pin is connected
to which microcontroller pin. A code snippet for a serial device:

    //The clock pin of the device is connected to pin 3:
    serial.applyPinMapping(APIN::SCK, 3);
    //The data pin is connected to pin 2:
    serial.applyPinMapping(APIN::SDA, 2);

## Compilation

The library itself is just a library, so it doesn't come with a compilation
framework. However, it relies on certain definitions to be present that are
set when compiling using the ucmf tool (see https://codeberg.org/ncc1988/ucmf).
The microcontroller family and model should be set using the UC\_FAMILY\_...
and UC\_MODEL\_... definitions:

    //for ATtiny45:
    UC_FAMILY_AVR
    UC_MODEL_ATTINY45

    //for 8051:
    UC_FAMILY_8051
    ...


## Examples

The "tests" folder contains test programs for the drivers that can be compiled
using either the ucmf tool and make, by writing a hand-crafted shell script or
some other fancy compilation process. They are written independent of any
particular microcontroller architecture.

## Documentation

The documentation is available on Codeberg.org:

https://codeberg.org/ncc1988/yauclib/wiki/Home

It is also available as git submodule of this repository. To download the
documentation, use the following git commands:

    git submodule init
    git submodule update

The documentation can then be found in the "doc" folder.
