/**
 * This file is part of the yauclib library
 *
 * Copyright (C) 2021 Moritz Strohm
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http s ://www.gnu.org/licenses/>.
 */


#include "../../Microcontroller.h"

//AVR-specific includes:
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>

//Set default values in case F_CPU is not set:
#ifndef F_CPU
    #if defined  UC_MODEL_ATTINY25 || defined UC_MODEL_ATTINY45 \
        || defined UC_MODEL_ATTINY85
        #define ATTINYx5
        #define F_CPU 1000000L
    #elif defined UC_MODEL_ATMEGA328P && defined UC_PACKAGE_ARDUINO_UNO
        #define F_CPU 16000000L
    #else
        #error "Unsupported AVR/Arduino microcontroller."
    #endif
#endif

#include <util/delay_basic.h>




extern "C" {
    /**
     * This function is called when a pure virtual function is called.
     * In that case, the microcontroller is put to power down sleep mode
     * with all interrupts cleared to save energy.
     */
    void __cxa_pure_virtual() {
        cli();
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable();
    }
}




/**
 * This helper function converts a physical pin a number to an AVR port number
 * (the "virtual" pin, reachable via a specific memory address).
 */
constexpr uint8_t mapPinToAvrPort(uint8_t pin)
{
    #if defined ATTINYx5
        #if defined UC_PACKAGE_PDIP || defined UC_PACKAGE_SOIC
        //Pin 4 is GND, pin 8 is VCC. All other ones belong to to port B.
        if (pin == 1) {
            return 1 << PB5;
        } else if (pin == 2) {
            return 1 << PB3;
        } else if (pin == 3) {
            return 1 << PB4;
        } else if (pin == 5) {
            return 1 << PB0;
        } else if (pin == 6) {
            return 1 << PB1;
        } else if (pin == 7) {
            return 1 << PB2;
        } else {
            //Invalid pin number: return 0
            return 0;
        }
        #elif defined UC_PACKAGE_QFN || defined UC_PACKAGE_MLF
        if (pin == 1) {
            return 1 << PB5;
        } else if (pin == 2) {
            return 1 << PB3;
        } else if (pin == 5) {
            return 1 << PB4;
        } else if (pin == 11) {
            return 1 << PB0;
        } else if (pin == 12) {
            return 1 << PB1;
        } else if (pin == 14) {
            return 1 << PB2;
        } else {
            //Invalid pin number: return 0
            return 0;
        }
        #else
            #error UC_PACKAGE parameter is missing or contains an invalid package name. For the ATTINYx5 microcontrollers, onyl PDIP, SOIC, QFN and MLF packages are supported.
        #endif
    #elif defined UC_MODEL_ATMEGA328P
        #if defined UC_PACKAGE_ARDUINO_UNO
        if (pin == 0) {
            return 1 << PD0;
        } else if (pin == 1) {
            return 1 << PD1;
        } else if (pin == 2) {
            return 1 << PD2;
        } else if (pin == 3) {
            return 1 << PD3;
        } else if (pin == 4) {
            return 1 << PD4;
        } else if (pin == 5) {
            return 1 << PD5;
        } else if (pin == 6) {
            return 1 << PD6;
        } else if (pin == 7) {
            return 1 << PD7;
        } else if (pin == 8) {
            return 1 << PB0;
        } else if (pin == 9) {
            return 1 << PB1;
        } else if (pin == 10) {
            return 1 << PB2;
        } else if (pin == 11) {
            return 1 << PB3;
        } else if (pin == 12) {
            return 1 << PB4;
        } else if (pin == 13) {
            return 1 << PB5;
        } else if (pin == 100) {
            //Pin A0
            return 1 << PC0;
        } else if (pin == 101) {
            //Pin A1
            return 1 << PC1;
        } else if (pin == 102) {
            //Pin A2
            return 1 << PC2;
        } else if (pin == 103) {
            //Pin A3
            return 1 << PC3;
        } else if (pin == 104) {
            //Pin A4
            return 1 << PC4;
        } else if (pin == 105) {
            //Pin A5
            return 1 << PC5;
        } else {
            //Invalid pin number
            return 0;
        }
        #endif
    #endif
}


constexpr bool isAnalogPin(uint8_t pin)
{
    #if defined ATTINYx5
        #if defined UC_PACKAGE_PDIP || defined UC_PACKAGE_SOIC
            return (pin == 1) || (pin == 2) || (pin == 3) || (pin == 7);
        #elif defined UC_PACKAGE_QFN || defined UC_PACKAGE_MLF
            return (pin == 1) || (pin == 2) || (pin == 5) || (pin == 14);
        #endif
    #elif defined UC_MODEL_ATMEGA328P
        #if defined UC_PACKAGE_ARDUINO_UNO
            return (pin >= 100) && (pin <= 105);
        #endif
    #endif
}


/**
 * Maps a pin to an analog input channel (ADCx).
 *
 * @returns uint8_t The analog input channel, starting from 0.
 *     255 is returned when the pin is not connected to an
 *     analog input channel.
 */
constexpr uint8_t mapAnalogInput(uint8_t pin)
{
    #if defined ATTINYx5
        #if defined UC_PACKAGE_PDIP || defined UC_PACKAGE_SOIC
            return (pin == 1)
                ? 0
                : (pin == 2)
                ? 3
                : (pin == 3)
                ? 2
                : (pin == 7)
                ? 1
                : 255;
        #elif defined UC_PACKAGE_QFN || defined UC_PACKAGE_MLF
            return (pin == 1)
                ? 0
                : (pin == 2)
                ? 3
                : (pin == 5)
                ? 2
                : (pin == 14)
                ? 1
                : 255;
        #endif
    #elif defined UC_MODEL_ATMEGA328P
        #if defined UC_PACKAGE_ARDUINO_UNO
            return (pin >= 100) && (pin <= 105)
                ? (pin - 100)
                : 255;
        #endif
    #endif
}


/**
 * Special fields for AVR microcontrollers.
 */
struct AVR
{
    static InterruptHandler* timer0_handler_a;
    static InterruptHandler* timer0_handler_b;
    static InterruptHandler* timer1_handler_a;
    static InterruptHandler* timer1_handler_b;


    static void handleTimer0a()
    {
        if (AVR::timer0_handler_a != nullptr) {
            AVR::timer0_handler_a->handleInterrupt();
        }
    }


    static void handleTimer0b()
    {
        if (AVR::timer0_handler_b != nullptr) {
            AVR::timer0_handler_b->handleInterrupt();
        }
    }


    static void handleTimer1a()
    {
        if (AVR::timer1_handler_a != nullptr) {
            AVR::timer1_handler_a->handleInterrupt();
        }
    }


    static void handleTimer1b()
    {
        if (AVR::timer1_handler_b != nullptr) {
            AVR::timer1_handler_b->handleInterrupt();
        }
    }
};


InterruptHandler* AVR::timer0_handler_a = nullptr;
InterruptHandler* AVR::timer0_handler_b = nullptr;
InterruptHandler* AVR::timer1_handler_a = nullptr;
InterruptHandler* AVR::timer1_handler_b = nullptr;


//Microcontroller class implementation:


//The ADC in AVR microcontrollers outputs values from 0 to 1023.
const uint16_t Microcontroller::analog_input_max_value = 1023;
//The clock speed is simply F_CPU / 1000 (to get the value in Kilohertz):
const uint32_t Microcontroller::clock_speed = F_CPU / 1000;


bool Microcontroller::setupAnalogReading()
{
    return true;
}



void Microcontroller::writePin(uint8_t pin, bool value)
{
    #if defined ATTINYx5
    //There is only port B, so we do not need to remember the
    //pin number (and by that save 1 byte of memory).
    pin = mapPinToAvrPort(pin);
    if (pin == 0) {
        //Invalid pin number. Do nothing.
        return;
    }
    if (value) {
        PORTB |= pin;
    } else {
        PORTB &= ~pin;
    }
    #elif defined UC_MODEL_ATMEGA328P
    uint8_t port_bit = mapPinToAvrPort(pin);
        #if defined UC_PACKAGE_ARDUINO_UNO
        if (pin < 8) {
            if (value) {
                PORTD |= port_bit;
            } else {
                PORTD &= ~port_bit;
            }
        } else if (pin < 14) {
            if (value) {
                PORTB |= port_bit;
            } else {
                PORTB &= ~port_bit;
            }
        } else if ((pin > 199) && (pin < 206)) {
            if (value) {
                PORTC |= port_bit;
            } else {
                PORTC &= ~port_bit;
            }
        }
        #endif
    #endif
}


void Microcontroller::togglePin(uint8_t pin)
{
    #if defined ATTINYx5
    //There is only port B, so we do not need to remember the
    //pin number (and by that save 1 byte of memory).
    pin = mapPinToAvrPort(pin);
    if (pin == 0) {
        //Invalid pin number. Do nothing.
        return;
    }
    PORTB ^= pin;
    #elif defined UC_MODEL_ATMEGA328P
    uint8_t port_bit = mapPinToAvrPort(pin);
        #if defined UC_PACKAGE_ARDUINO_UNO
        if (pin < 8) {
            PORTD ^= port_bit;
        } else if (pin < 14) {
            PORTB ^= port_bit;
        } else if ((pin > 199) && (pin < 206)) {
            PORTC ^= port_bit;
        }
        #endif
    #endif
}


void Microcontroller::writeAnalogPin(uint8_t pin, uint16_t value)
{
    
}


bool Microcontroller::readPin(uint8_t pin)
{
    #if defined ATTINYx5
    //There is only port B, so we do not need to remember the
    //pin number (and by that save 1 byte of memory).
    pin = mapPinToAvrPort(pin);
    if (pin == 0) {
        //Invalid pin number. Assume it is set to zero.
        return false;
    }
    return (PINB & pin) != 0;
    #elif defined UC_MODEL_ATMEGA328P
    uint8_t port_bit = mapPinToAvrPort(pin);
        #if defined UC_PACKAGE_ARDUINO_UNO
        if (pin < 8) {
            return (PIND & port_bit) != 0;
        } else if (pin < 14) {
            return (PINB & port_bit) != 0;
        } else if ((pin > 199) && (pin < 206)) {
            return (PINC & port_bit) != 0;
        }
        #endif
    #endif
}


uint16_t Microcontroller::readAnalogPin(uint8_t pin, bool normalise8)
{
    //Check which pin shall be read:
    if (!isAnalogPin(pin)) {
        //Digital pins cannot be read using an analog pin.
        return 0;
    }
    uint8_t adc_number = mapAnalogInput(pin);
    #if defined ATTINYx5
        //Use VCC as reference.
        ADMUX = adc_number & 0x0F;
        if (normalise8) {
            ADMUX |= 1 << ADLAR;
        }
    #elif defined UC_MODEL_ATMEGA328P
        //Use VCC as reference (needs a capacitor at the AREF pin).
        //Enable the analog channel that shall be read.
        ADMUX = 0x40 | (adc_number & 0x0F);
        if (normalise8) {
            ADMUX |= 1 << ADLAR;
        }
    #endif
    //Configure the analog to digital conversion:
    ADCSRA = 0x87;
    //Start the conversion:
    ADCSRA |= 1 << ADSC;
    //Wait until the conversion should be finished:
    while ((ADCSRA & (1 << ADSC)) != 0) {
        Microcontroller::waitMicroseconds(1);
    }
    //Disable the ADC:
    ADCSRA = 0x00;

    //Read the result into one integer and return that:
    if (normalise8) {
        //Just return the highest 8 bits:
        return ADCH;
    } else {
        return ADCL | (ADCH << 8);
    }
}


void Microcontroller::setPinDirection(uint8_t pin, bool output)
{
    #if defined ATTINYx5
    //There is only port B, so we do not need to remember the
    //pin number (and by that save 1 byte of memory).
    pin = mapPinToAvrPort(pin);
    if (pin == 0) {
        //Invalid pin number. Do nothing.
        return;
    }
    if (output) {
        DDRB |= pin;
    } else {
        DDRB &= ~pin;
    }
    #elif defined UC_MODEL_ATMEGA328P
    uint8_t port_bit = mapPinToAvrPort(pin);
        #if defined UC_PACKAGE_ARDUINO_UNO
        if (pin < 8) {
            if (output) {
                DDRD |= port_bit;
            } else {
                DDRD &= ~port_bit;
            }
        } else if (pin < 14) {
            if (output) {
                DDRB |= port_bit;
            } else {
                DDRB &= ~port_bit;
            }
        } else if ((pin > 199) && (pin < 206)) {
            if (output) {
                DDRC |= port_bit;
            } else {
                DDRC &= ~port_bit;
            }
        }
        #endif
    #endif
}


void Microcontroller::wait(const uint16_t miliseconds)
{
    if (miliseconds < 1) {
        //Nothing to do.
        return;
    }
    constexpr uint32_t loops_per_milisecond = F_CPU / 4 / 1000;
    uint32_t loops = (loops_per_milisecond * miliseconds);

    //NOTE: integer division did not work for AVR,
    //therefore, the value is substracted on each run:
    while (loops >= 65536) {
        _delay_loop_2(0);
        loops -= 65536;
    }
    if (loops > 0) {
        _delay_loop_2(loops);
    }
}


void Microcontroller::waitMicroseconds(const uint16_t microseconds)
{
    if (microseconds < 1) {
        //Nothing to do.
        return;
    }
    constexpr uint32_t loops_per_microsecond = F_CPU / 3 / 1000000;
    uint16_t loops = (loops_per_microsecond * microseconds);

    //NOTE: integer division did not work for AVR,
    //therefore, the value is substracted on each run:
    while (loops >= 256) {
        _delay_loop_1(0);
        loops -= 256;
    }
    _delay_loop_1(loops);
}



/**
 * UNSTABLE METHOD!
 * Adds an interrupt handler that gets called when a timer
 * counted a specific amount of microseconds.
 */
void Microcontroller::addTimerInterruptHandler(
    InterruptHandler* handler,
    uint16_t microseconds,
    bool hi_res_timer
    )
{
    if (hi_res_timer) {
        if (AVR::timer1_handler_a == nullptr) {
            //Use timer 1 A
            AVR::timer1_handler_a = handler;
        } else {
            //Use timer 1 B
            AVR::timer1_handler_b = handler;
        }
    } else {
        if (AVR::timer0_handler_a == nullptr) {
            //Use timer 0 A
            AVR::timer0_handler_a = handler;
        } else {
            //Use timer 0 B
            AVR::timer0_handler_b = handler;
        }
    }
}


void Microcontroller::powerOff()
{
    power_all_disable();
}
